import React from 'react';
import Page1 from './pages/Page1';
import Page2 from './pages/Page2';
import './App.css';

function App() {
  return (
    <div className="App">
      <Page1/>
      <Page2/>
    </div>
  );
}

export default App;
