import React from 'react';
import  ReactDOM   from 'react-dom';
import { Provider } from 'react-redux';
import { store } from './redux/store';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import { createBrowserHistory } from 'history';
import Container1  from "./redux/container/Container1";
import Container2  from "./redux/container/Container2";
import Container3  from "./redux/container/Container3";
export const history = createBrowserHistory();
const basePath = window.location.pathname;

ReactDOM.render(
    
    <Provider store={store}>
        <div>
            <Router basename={basePath}>
                <Route exact path = '/' component={Container1}></Route>
                <Route exact path = '/verify' component={Container2}></Route>
                <Route exact path = '/completion' component={Container3}></Route>
            </Router>
        </div>

    </Provider>,
    document.getElementById('root')
);
