import React from "react";
import { shallow, mount } from "enzyme";
import Page1 from "./Page1";
import Container1 from "../redux/container/Container1";

describe("Page1 renders correctly", () => {
  it("Page1 renders ", () => {
    const component = shallow(<Container1 />);

    // expect(component).toMatchSnapshot();
    expect(component.props().isLoading).to.equal(false);
  });
});
