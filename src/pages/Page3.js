import React from "react";
import { useEffect } from "react";
import { history } from "../index";
import SubPage from "./Pagesub"


const newArrayPage3Data={
    sampleName: 'renu Prasad',
    sex: 'male'
};

const Page3 = ({setpag3data, setpag3dataApi,newDataApiHere, ...props }) => {
  useEffect(() => {
    setpag3data(newArrayPage3Data);
    setpag3dataApi();
    
  }, []);
  console.log(newDataApiHere );
  const handleclick = () => {
    
    history.push("/verify");
  };
  const army= (newDataApiHere || []).map((item, index) => {
    return (
      <th key={Math.random()}>
        <h4>{item.title}</h4>
      </th>
    );
  });
  return <div onClick={handleclick}>Sample page 3 
  <SubPage army={army}/>
  
  </div>;
};

export default Page3;
