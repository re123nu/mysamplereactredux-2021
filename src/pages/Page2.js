import React, { Component, useEffect, useState } from "react";
import { history } from "../index";
import SubPage from "./Pagesub";
import { useForm } from 'react-hook-form';

// const { register, handleSubmit, errors } = useForm(); // initialize the hook

const Page2 = ({ getData2, newDataApi, isLoading, page1Data,page3data,page3ApiData,match,children, ...props }) => {
  useEffect(() => {
    // getData2();
    console.log(page1Data);
    console.log(isLoading);
    console.log(newDataApi);
    console.log(page3data);
    console.log(page3ApiData);
    console.log("MATCH..........", match.isExact);
    //callback();
  }, []);
  const { register, handleSubmit, errors } = useForm(); // initialize the hook
  console.log(props);
  if (isLoading) return <div>LOADING......</div>;

  const some= "Print My Name";

  const data = (newDataApi || []).map((item, index) => {
    return (
      <th>
        <h4 key={Math.random()}>{item.title}</h4>
      </th>
    );
  });

  console.log(page1Data);
  const callback = (count) => {
    
    // do something with value in parent component, like save to state
    alert(count);
    //console.log(children);
    
}
const handleclickback = () => {
    
  props.history.push("/");
};

  
  const onSubmit = (data) => {
    console.log(data);
  };
//console.log(children);
//console.log(page1Data.firstname);
  return (
    <div>
      Page2222222222222222222222222222
      <SubPage myName={some} parentCallback={callback} />
      <table>
        <thead>
          <tr>{data}</tr>
        </thead>
      </table>
      {page1Data? page1Data.firstname: "No Data"}{page1Data?page1Data.lastname: ""}{page1Data? page1Data.age: "" }
      <form onSubmit={handleSubmit(onSubmit)}>
      <input name="firstname" ref={register({ required: true })} /> {/* register an input */}
      {errors.firstname && 'First name is required.'}
      <input name="lastname" ref={register({ required: true })} />
      {errors.lastname && 'Last name is required.'}
      <input name="age" ref={register({ pattern: /\d+/ }&&{ required: true })} />
      {errors.age && 'Please enter number for age.'}
      <input type="submit" />
    </form>
    <p onClick={handleclickback}>back</p>
    </div>
  );
};

export default Page2;
