import React, { Component, useEffect, useState } from "react";
import { history } from "../index";
import { useForm } from 'react-hook-form';
const arryData = {
  empID: 21,
  contryID: 41,
  title: "MyCOMPHCL INFOSYS ANY",
};
const myDatahere = (some) => {
  return some;
};

const Page1 = ({ getData, newDataApi, isLoading, setdata, isWe,page1Data, ...props }) => {
  useEffect(() => {
    getData();
    console.log("is weeeee", isWe);
    
  }, []);
  
  let  preloadedValues  = {};
  if(page1Data){
    console.log(page1Data);
    preloadedValues  = page1Data;

}
  const { register, handleSubmit, errors } = useForm({

    defaultValues: preloadedValues
  }); // initialize the hook
  const handleclick = () => {
    // setdata(arryData);
    props.history.push("/verify");
  };

  console.log(props);
  if (isLoading) return <div>LOADING......</div>;

  const data = (newDataApi || []).map((item, index) => {
    return (
      <th key={Math.random()}>
        <h4>{item.title}</h4>
      </th>
    );
  });

  const onSubmit = (data) => {
    console.log(data);
    setdata(data);
    props.history.push("/verify");
  };

  const country = [
    { value: 'USA', name: 'USA1' },
    { value: 'CANADA', name: 'CANADA1' },
    { value: 'INDIA', name: 'INDIA1' }  , 
    { value: 'Singapore', name: 'CANSingaporeADA1' }  ,             
];

const datadrop = (country || []).map((item, index) => {
  return (
      <option value={item.value}>{item.name}</option>
  );
});
console.log(datadrop);
console.log(datadrop[0].props.children);
  return (
    <div>
      <table>
        <thead>
          <tr>{data}</tr>
        </thead>
      </table>
      <p onClick={handleclick}>clickhere</p>
      /* My Changes ---- myrenu -------*/
      /* My Changes ---- myrenu2 */
      <label for="file">Downloading progress:</label>
<progress id="file" value="20" max="100"> 32% </progress>

      <form onSubmit={handleSubmit(onSubmit)}>
      <input name="firstname" ref={register({ required: true })} /> {/* register an input */}
      {errors.firstname && 'First name is required.'}
      <input name="lastname" ref={register({ required: true })} />
      {errors.lastname && 'Last name is required.'}
      <input name="age" ref={register({ pattern: /\d+/ }&&{ required: true })} />
      {errors.age && 'Please enter number for age.'}
      <input type="radio" name="vehicles" value="1"
          ref={register({ required: true })} className="radio" />
        <label class="radio">1</label>
        <input type="radio" name="vehicles"  value="2"
          ref={register({ required: true })} className="radio" />
        <label class="radio">2</label>
        {errors.vehicles && 'Please Radio'}
        <select ref={register} name="ageGroup">
        {datadrop}
        </select>
      <input type="submit" />
    </form>
    </div>
  );
};

export default Page1;
