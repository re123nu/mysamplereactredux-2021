import { connect } from "react-redux";

import Page2 from "../../pages/Page2";
import { getJsonData } from "../action/Action1";

export const mapStateToProps = (state) => ({
  newDataApi: state.setDataReducer.data,
  isLoading: state.setDataReducer.isLoading,
  page1Data: state.setDataReducer.data2,
  page3data: state.dataNewReducer.data3,
  page3ApiData: state.dataNewReducer.data,
});

export const mapDispatchToProps = (dispatch) => ({
  getData2: () => dispatch(getJsonData()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Page2);
