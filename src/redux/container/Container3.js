import { connect } from "react-redux";

import Page3 from "../../pages/Page3";
import {getPage3Data, getPage3DataAPI} from '../action/Action3'

export const mapStateToProps = (state) => ({
newDataApiHere: state.dataNewReducer.data


});

export const mapDispatchToProps = (dispatch) => ({

    setpag3data: (data) => dispatch(getPage3Data(data)),
    setpag3dataApi: () => dispatch(getPage3DataAPI()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Page3);

// import { connect } from "react-redux";

// import Page1 from "../../pages/Page1";
// import { getJsonData, getMyData } from "../action/Action1";

// export const mapStateToProps = (state) => ({
//   newDataApi: state.setDataReducer.data,
//   isLoading: state.setDataReducer.isLoading,
//   isWe: state.setDataReducer.isWe,
// });

// export const mapDispatchToProps = (dispatch) => ({
//   getData: () => dispatch(getJsonData()),
//   setdata: (data) => dispatch(getMyData(data)),
// });

// export default connect(mapStateToProps, mapDispatchToProps)(Page1);

