import { connect } from "react-redux";

import Page1 from "../../pages/Page1";
import { getJsonData, getMyData } from "../action/Action1";

export const mapStateToProps = (state) => ({
  newDataApi: state.setDataReducer.data,
  isLoading: state.setDataReducer.isLoading,
  isWe: state.setDataReducer.isWe,
  page1Data: state.setDataReducer.data2,
});

export const mapDispatchToProps = (dispatch) => ({
  getData: () => dispatch(getJsonData()),
  setdata: (data) => dispatch(getMyData(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Page1);
