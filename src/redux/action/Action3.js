


import { PAGE3DATA, PAGE3APIDATA, PAGE3APIDATASUCCESS } from "../constants"

export const getPage3Data=(data3)=>({
    type:PAGE3DATA,
    data3
})

export const getPage3DataAPI =()=>({
    type:PAGE3APIDATA,
    
})

export const getPage3DataAPISuccess =(data)=>({
    type:PAGE3APIDATASUCCESS,
    data
})