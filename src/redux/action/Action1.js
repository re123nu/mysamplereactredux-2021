import {
  JSONDATA,
  JSONDATASUCCESS,
  JSONDATAFAILURE,
  MYDATAHERE,
} from "../constants";

export const getJsonData = () => ({
  type: JSONDATA,
});

export const getJsonDataSuccess = (data) => ({
  type: JSONDATASUCCESS,
  data,
});

export const getJsonDataFailure = (error) => ({
  type: JSONDATAFAILURE,
  error,
});

export const getMyData = (data2) => {
  debugger;
  return {
    type: MYDATAHERE,
    data2,
  };
};
