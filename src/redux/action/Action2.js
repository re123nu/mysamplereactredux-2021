
import { JSONDATA , JSONDATASUCCESS, JSONDATAFAILURE} from "../constants";



export const getJsonData=(data)=>({
    type:JSONDATA
})

 export const getJsonDataSuccess=(data)=>({
    type:JSONDATASUCCESS,
    data
})


export const getJsonDataFailure=(error)=>({
    type:JSONDATAFAILURE,
    error
})

