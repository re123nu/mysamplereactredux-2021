import { PAGE3DATA , PAGE3APIDATA, PAGE3APIDATASUCCESS} from '../constants'

const initialState = {
    data: [],
    inProgress:false
}

export default function dataNewReducer(state=initialState, action){
    switch (action.type) {
        case PAGE3DATA:
            return{
                ...state,
                inProgress: true,
                data3:action.data3
            }

            case PAGE3APIDATA:

                return{
                    ...state,
                    inProgress: false,
                    data: [],
                }

                case PAGE3APIDATASUCCESS:

                return{
                    ...state,
                    inProgress: true,
                    data:action.data
                }

            default:
                return state
    }
}


  