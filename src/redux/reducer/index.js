import { combineReducers } from "redux";
import setDataReducer from './reducer1'
import dataNewReducer from './reducer3';

const rootReducer= combineReducers({setDataReducer, dataNewReducer});

export default rootReducer;