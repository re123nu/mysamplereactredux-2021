import {
  JSONDATA,
  JSONDATASUCCESS,
  JSONDATAFAILURE,
  MYDATAHERE,
} from "../constants";
import { fromJS } from "immutable";

const initialState1 = fromJS({
  dataState: {
    data: [],
    isLoading: false,
    new: {
      deep: false,
    },
  },
});

const initialState = {
  data: [],
  isLoading: false,
  isWe: false,
};
export default function setDataReducer(state = initialState, action) {
  switch (action.type) {
    case JSONDATA:
      return {
        ...state,
        isLoading: true,
        data: [],
      };

    case JSONDATASUCCESS:
      return {
        ...state,
        isLoading: false,
        data: action.data,
      };

    case JSONDATAFAILURE:
      return {
        ...state,
        isLoading: false,
        data: [],
      };

    case MYDATAHERE:
      return {
        ...state,
        isLoading: false,
        data2: action.data2,
      };

    default:
      return state;
  }
}
