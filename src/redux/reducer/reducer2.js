import { JSONDATA, JSONDATASUCCESS, JSONDATAFAILURE } from '../constants'
const initialState = {
    data:[],
    isLoading:false
}
export default function setDataReducer(state=initialState, action){
    switch(action.type){
        case JSONDATA: 

            return{
                ...state,
                isLoading:true,
                data:[]
            }

            case JSONDATASUCCESS: 

            return{
                ...state,
                isLoading:false,
                data:action.data
            }


            case JSONDATAFAILURE: 

            return{
                ...state,
                isLoading:false,
                data:[]
            }


            default:
            return state

    }

}