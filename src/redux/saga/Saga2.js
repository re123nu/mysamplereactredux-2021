import axios from 'axios'
import {  put,  call, delay} from 'redux-saga/effects';
import { JSONDATAFAILURE, JSONDATASUCCESS } from '../constants';

export default function* getDataSaga(){
    try {
        //yield delay(5000)
        const responsedata =  yield call(()=> axios.get('https://jsonplaceholder.typicode.com/posts'))
        
        yield put({ 
            type: JSONDATASUCCESS,
            data: responsedata.data
        })

    } catch(error){
        alert('inside error')
        yield put({
            type: JSONDATAFAILURE,
            data: error.messahe
        })
    }
}


