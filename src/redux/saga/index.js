import { all, takeLatest } from "redux-saga/effects";
import { JSONDATA, MYDATAHERE,PAGE3DATA, PAGE3APIDATA} from "../constants"
import {getDataSaga, sendDataSaga} from './Saga1';
import { sendDataSagaPage3, sendDataSaga3ApiGet } from './Saga3';

function* watcher() {

    yield all ([takeLatest(MYDATAHERE,sendDataSaga ),
                takeLatest(JSONDATA,getDataSaga ),
                takeLatest(PAGE3DATA,sendDataSagaPage3 ),
                takeLatest(PAGE3APIDATA,sendDataSaga3ApiGet )
    ])
}

export default function* rootSaga() {
    yield all([watcher()]);
}
