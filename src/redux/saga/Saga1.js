import axios from "axios";
import { put, call, delay } from "redux-saga/effects";
import { JSONDATAFAILURE, JSONDATASUCCESS, MYDATAHERE } from "../constants";

export function* getDataSaga() {
  try {
    //yield delay(5000)
    alert();
    debugger;
    const responsedata = yield call(() =>
      axios.get("https://jsonplaceholder.typicode.com/posts")
    );

    yield put({
      type: JSONDATASUCCESS,
      data: responsedata.data,
    });
  } catch (error) {
    alert("inside error");
    yield put({
      type: JSONDATAFAILURE,
      data: error.messahe,
    });
  }
}
export function* sendDataSaga(action) {
  debugger;
  try {
    //yield delay(5000)

    console.log(action);
    alert("inside send saga");
    const responsedata = yield call(() =>
      axios.post("http://localhost:3000/posts", action.data2)
    );

    // yield put({
    //   type: MYDATAHERE,
    //   data: responsedata.data,
    // });
  } catch (error) {
    alert("inside error");
  }
}
